# Welcome BITS Candidates
If you're reading this, you've passed your interview, so Congrats! Now its time to show us that you can code and can fill the backend position available.

The task is simple, build an endpoint/service using PHP with no external dependencies other than the ones provided: [Slim Framework 4](https://www.slimframework.com/docs/v4/). This means you really have to think about your code and how to structure the app. Laravel will not come to your rescue. 

We are not reeinventing the wheel here, we just need to see your understanding of what "good code" is and how you approach the design of your software. Anyone can write code, but only good developers can write good and well designed code. Also, we've taken you out of your comfort zone.

&nbsp;
&nbsp;

---
## Preparing The Environment
---
The entire task is prepared to you in a dockerized container. We spent hours preparing this so you dont.  You need to install docker and docker-compose before you begin working:

- https://docs.docker.com/desktop/
- https://docs.docker.com/compose/install/

Once the installation is complete, open up a command line or a terminal and run:

`
docker-compose up
`

The first time you run this command, building the environment can take a few minutes.

Once the environments are up, you will see the messages (could be in separate locations): 

```
app_1 | APP is now Ready to use
db_1  | mysqld: ready for connections.
```

Once this message is displayed, open your favorite browser (or terminal) and go to: http://localhost:8080

&nbsp;
&nbsp;

---
## Where to write your code
---
The docker-compose mounts the app directory to the docker container, so you can start writing code directly in the app folder. The `index.php` has a single function welcoming you and shows how you can access the `database` service. This is all you need to get started.

A single test is also written and prepared for you under the tests directory. To run the test you can use `bin/phpunit`. If you do not have PHP installed on your host, you can enter the docker container `docker exect -it` and run it from there.

Code is immediately refreshed on the webserver, you do not have to restart the containers or services.

&nbsp;
&nbsp;

---
## The Task
---
We have created a small database for an ecommerce/store that has customers, orders, order items, payments and sellers.

Your task is to build the REST endpoints for the following services:
1) Create a new Service that creates users: &nbsp;&nbsp; `POST /accounts`
+ Customers need to login to view their orders. However, to do so, they need to first register.
+ Store the new users into the table `accounts`. The fields required are, `customer_id, username, password.`
+ Endpoint should expect atleast a username, a password and a valid customer id from the customers table.

&nbsp;

2) Fetch all customers orders using the Customer ID &nbsp;&nbsp; `GET /customers/:id/orders`
+ You shouldnt allow anyone to fetch any customers information. You should ensure proper authentication in the request headers.
+ Result should be an array of orders with the structure defined below in point #3.

&nbsp;

3) Fetch a customers specific order using the customer id and order id &nbsp;&nbsp; `GET /customers/:id/orders/:oid`
+ The result should be in the following format:
```json
{
    "id" : "",
    "status" : "",
    "purchased_at" : "",
    "pickedup_at" : "",
    "delivered_at" : "",
    "items" : [
        {
            "price" : "",
            "freight" : "",
            "product" : {
                "category" : "",
                "weight" : ""
            },
            "seller" : {
                "city" : "",
                "state" : ""
            }
        }
    ],
    "payment" : {
        "method" : "",
        "amount" : ""
    }
}
```

&nbsp;

4) Create a new order for an existing customer &nbsp;&nbsp; `POST /customers/:id/orders`
+ Request Headers must have valid authentication
+ Default order status should be 'submitted'
+ No Payment is expected
+ A list of valid product ids should be sent as the 'order items'
+ Ensure correct response headers are sent
+ Return should be the same as point #3

&nbsp;

5) Create a payment for an order and update its status &nbsp;&nbsp; `POST /customers/:id/orders/:oid/payments`
+ A payment should have a method and an amount
+ Order status should be updated to 'processing'
+ Result should be the same as point #3


&nbsp;
&nbsp;


---
## Tables & Structure
---
The entire database scripts is in the `db/db.sql` file. A simple layout is as follows:
1. `customers` joins with `accounts` on `accounts.customer_id` = `customers.id`
2. `orders` joins with `customers` on `orders.customer_id` = `customers.id`
3. `order_payments` joins with `orders` on `order_payments.order_id` = `orders.id`
3. `order_items` joins with `orders` on `order_items.order_id` = `orders.id`
3. `order_items` joins with `products` on `order_items.product_id` = `products.id`
3. `order_items` joins with `sellers` on `order_items.seller_id` = `sellers.id`

You can connect to the database to browse the data using adminer: http://localhost:8080/adminer.php

&nbsp;
&nbsp;

---
## What will I be assessed on
---
1. If your code works
2. If you wrote tests
3. Your application structure
4. How you modeled the entities and relations
5. How complicated is your code, can a junior understand what you did without help 
6. How long it took you to complete the task

&nbsp;
&nbsp;

---
## Questions
---
You can share any question/concern you have on teams. Not all your questions maybe answered, sometimes you may have to assume and justify your decisions.

&nbsp;
&nbsp;


># Good Luck!