-- MySQL dump 10.13  Distrib 5.7.35, for Linux (x86_64)
--
-- Host: localhost    Database: bits_store
-- ------------------------------------------------------
-- Server version       5.7.35

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `accounts`
--

DROP TABLE IF EXISTS `accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accounts` (
  `customer_id` varchar(32) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(100) NOT NULL,
  PRIMARY KEY (`customer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounts`
--

LOCK TABLES `accounts` WRITE;
/*!40000 ALTER TABLE `accounts` DISABLE KEYS */;
/*!40000 ALTER TABLE `accounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customers` (
  `id` varchar(32) NOT NULL,
  `zip_code` varchar(10) NOT NULL,
  `city` varchar(100) NOT NULL,
  `state` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customers`
--

LOCK TABLES `customers` WRITE;
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
INSERT INTO `customers` VALUES ('011671897080a38fa57f3c190a310d49','07176','guarulhos','SP'),('0ca135c2d4b81bb0234b5db12d9b7a8f','72006','brasilia','DF'),('1681471e9172d7e12ba08e9ac9e8628b','20250','rio de janeiro','RJ'),('24dd6be58b4717018fa8062703561b76','21215','rio de janeiro','RJ'),('2d8748fb35d51c2fd273ee67fff78b7a','80010','curitiba','PR'),('377e81546d7d92c97c710a231e747c45','18147','aracariguama','SP'),('3a38cfdf41dbda6098abeacd30ec2d3e','23860','mangaratiba','RJ'),('4b24f5ef0c134fe4d56bb392b3397dcd','25615','petropolis','RJ'),('564f0d69cba2da3f21f9017402d0dc06','05754','sao paulo','SP'),('573e026da782d51277f320fc94a8d4e8','38800','sao gotardo','MG'),('5f454675eda0812f4ca038d7a6796a38','90050','porto alegre','RS'),('60840dcf2701955cedc2aff564f80c3e','36730','pirapetinga','MG'),('641d5bf17c23bb54add76ce8e47c1bd2','02466','sao paulo','SP'),('6f1b9db6eb561df558883985dbd941fc','24460','sao goncalo','RJ'),('71b3c28ced6fa14321a92998ecd717e2','86801','apucarana','PR'),('739ee2d2d5960609ad780937fd9fa282','89888','caibi','SC'),('78463bfeb649747e91ee9a424d0edc3c','31720','belo horizonte','MG'),('7c94da97db6fe83e123f80baab533a97','82030','curitiba','PR'),('804751ae168c4c4d6d5178553e544300','42700','lauro de freitas','BA'),('92ac4160874c75b187c8222aec92e1a6','09895','sao bernardo do campo','SP'),('934a9204aacc7279a9db0896b83eb42e','03306','sao paulo','SP'),('a820146409b46b535505aff7d58f3ffc','03510','sao paulo','SP'),('ad4950aded55c2ea376be59506456d68','07729','caieiras','SP'),('b383a778278d061c8b275a611c26ca8a','06210','osasco','SP'),('b5983676b44fc48efb58264d12ffce30','02035','sao paulo','SP'),('bcce684969990851024216985d1a9b5c','88063','florianopolis','SC'),('c56ac9e8772a402f6a167237b0c13f28','19020','presidente prudente','SP'),('cd1e26832990fe789ebf64b0bd3de6c1','04374','sao paulo','SP'),('ce5e2dbebc82fdeb89bb35dfaddd56e2','07752','cajamar','SP'),('d1d326edcb5fc3e9aadd6e86b7a57910','02435','sao paulo','SP'),('e0f38b37c6f511a9d6219102bb2e932d','38413','uberlandia','MG'),('e3f8a41dc8def48711c086dad89ab9fd','44695','capim grosso','BA'),('e7f806cb2cffddb308ed9bfd6bf5c5fd','45810','porto seguro','BA'),('e957d597f76d275f40a848397fb6c7c3','09530','sao caetano do sul','SP'),('f90389504400442cf00ae1b04e8280a9','08080','sao paulo','SP'),('fd10113c54f4f4bfcaacbd1c2d188a77','30575','belo horizonte','MG');
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_items`
--

DROP TABLE IF EXISTS `order_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_items` (
  `order_id` varchar(32) NOT NULL,
  `order_item_id` varchar(32) NOT NULL,
  `product_id` varchar(32) NOT NULL,
  `seller_id` varchar(32) NOT NULL,
  `shipping_date` datetime DEFAULT NULL,
  `price` decimal(8,2) DEFAULT NULL,
  `freight` decimal(8,2) DEFAULT NULL,
  KEY `order_item_order_idx` (`order_id`),
  KEY `order_item_product_idx` (`product_id`),
  KEY `order_item_seller_idx` (`seller_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_items`
--

LOCK TABLES `order_items` WRITE;
/*!40000 ALTER TABLE `order_items` DISABLE KEYS */;
INSERT INTO `order_items` VALUES ('011f9dff2545a2cf8ac1809faed3ec88','1','8d1224321dc883703df71dcb5be296b3','cca3071e3e9bb7d12640c9fbe2301306','2017-08-07 19:10:21',109.73,16.53),('02f2f6063eb01b5e6e29bda347635389','1','17a019676883dce326999c11a46a14f0','9f505651f4a6abe901a56cdc21508025','2018-03-26 03:10:37',49.00,7.39),('04a59fe46fc5aa341da5378c195ed53c','1','2a15783d3cb601f8d9a6a86806b037b3','855668e0971d4dfd7bef1b6a4133b41b','2017-12-04 09:31:11',60.00,15.18),('053fdca16d1f69bd52f908f070a19049','1','ed2067a9c1f79553088a3c67b99a9f97','cbd996ad3c1b7dc71fd0e5f5df9087e2','2017-10-17 15:07:24',56.97,12.74),('05f3fc274d71ebbd552966d716c9039d','1','94774f52c71a3e4ce084c21c873507cc','bd697db56ff8e7c9cd1abeb7f72029b2','2018-05-16 19:01:11',159.99,7.98),('084051935208c688c8cd5fae3d15810d','1','89db899bdb35ae40d0901e38f21d2ede','2a1348e9addc1af5aaa619b1a3679d6b','2017-03-02 15:46:49',44.90,17.78),('08915d73ff742886f0b4c9429af3c959','1','16ed6a6e3fce23b741650437fe58d65b','e5a38146df062edaf55c38afa99e42dc','2018-04-30 04:31:25',89.18,8.98),('09f58c00f941827ab206de7796785e44','1','55ba658d5c2f0b1f118f98c36fe3fa04','48efc9d94a9834137efd9ea76b065a38','2017-01-09 16:05:07',8.90,8.72),('0a0b0479c9e05814f250c7bea5b1a95c','1','fbc1488c1a1e72ba175f53ab29a248e8','ef0ace09169ac090589d85746e3e036f','2018-06-20 21:30:45',119.50,17.77),('0a0b0479c9e05814f250c7bea5b1a95c','2','fbc1488c1a1e72ba175f53ab29a248e8','ef0ace09169ac090589d85746e3e036f','2018-06-20 21:30:45',119.50,17.77),('0acfe927eda045512670dbbf763b330a','1','5f2641478d7b5ffbfcf1f2b5cdff4709','b33aaadd1a8891d2dfef0c4c3bdf0371','2018-04-26 15:30:27',100.00,14.79),('0d2d1445891e7aba669fa2069d695601','1','72d3bf1d3a790f8874096fcf860e3eff','0bae85eb84b9fb3bd773911e89288d54','2017-10-18 04:49:33',38.25,13.50),('0daf1f0e67b534ad873ba61c5d6ad7d3','1','1f30aacc69904c0d0bf8d05cb2abef72','4a3ca9315b744ce9f8e9374361493884','2017-09-13 11:03:50',135.00,18.20),('0e5f3862d95c44aabc9b2a0f703f9f75','1','126abb319a746b23254ef06ddf10181d','6560211a19b47992c3666cc44a7e94c0','2018-02-15 23:50:30',209.00,16.21),('0f95a7e23723d5388a494f2d812a8332','1','65266b2da20d04dbe00c5c2d3bb7859e','2c9e548be18521d1c43cde1c582c6de8','2018-04-05 20:08:34',19.90,15.23),('10f69c462a2c700cf1b141b9874af717','1','ec529a51d3a0832578dc8ca51574d2bd','5c853bb56f70f4d14218944bae111d7a','2018-07-25 16:42:33',12.99,7.39),('1194afa31e54aeee123be8df42e3f573','1','99a4788cb24856965c36a24e339b6058','4a3ca9315b744ce9f8e9374361493884','2018-06-13 17:15:38',79.90,14.65),('12e6f847bdf86df79a51931bbd0798b9','1','d103baa8d35783ff2772704ed17ddd45','6560211a19b47992c3666cc44a7e94c0','2017-07-20 21:50:16',59.00,14.16),('1314353395f7148ba64400eab34bb50c','1','6f33a4a09ae1180a0ee1ff4682b2d21f','725c32fa80c2faacc4fc88450d27314e','2017-12-01 09:33:29',139.99,16.74),('131bcb82ecd96d251fa7eb8bb1f1baca','1','097d5a4bed2be012b4e46e6f5ae0694b','17eea220a40cc0d2c0c5346379682398','2017-09-20 10:45:13',358.99,16.26),('1355bd6c7fa80ea43bdecab48ff8052c','1','3a44e1cd6610483f2ab5e0ca8c2a4b45','ac3508719a1d8f5b7614b798f70af136','2017-12-01 01:37:23',69.90,15.25),('1422a4b4a2aa8b66fd49ff5557200b42','1','8bb27b1d96be90b36b8d0c7f30931d52','4bfc7a4a1cf8d4d2121c27422d9e50b5','2018-06-05 14:30:56',280.00,28.04),('1422a4b4a2aa8b66fd49ff5557200b42','2','8bb27b1d96be90b36b8d0c7f30931d52','4bfc7a4a1cf8d4d2121c27422d9e50b5','2018-06-05 14:30:56',280.00,28.04),('1422a4b4a2aa8b66fd49ff5557200b42','3','8bb27b1d96be90b36b8d0c7f30931d52','4bfc7a4a1cf8d4d2121c27422d9e50b5','2018-06-05 14:30:56',280.00,28.04),('14de412ae0efc05d642771ab3bff5da6','1','b8ca27a8b7658a95a8040f0a9489c952','b9ca8e8baa5d4aa038394a700f63e69f','2018-04-27 15:31:22',155.99,17.91),('14e29f83cf6fcd5fcca40380a248eea1','1','9f7b48e2d4e4d7375f71a10791faa1b8','259f7b5e6e482c230e5bfaa670b6bb8f','2018-02-19 11:30:27',59.90,13.08),('1594012ccc1b0770373ce691d697e5ae','1','b6c312bb8b2e0c3ab0a23d207802f82f','723cd880edaacdb998898b67c8f9da30','2018-04-27 19:31:42',330.00,32.17),('16ec5f5b0b5245798a6334e4c239d13a','1','a62e25e09e05e6faf31d90c6ec1aa3d1','634964b17796e64304cadf1ad3050fb7','2018-01-26 14:16:44',108.00,15.52),('178358411049ad27f877b8964564528a','1','d02e5b014cc50a42a3ede0b3dc1fc63e','3785b653b1b82de85ab47dd139938091','2018-03-09 10:30:47',59.90,14.15),('17afedb7e5fad837f54bacf5c66c99f8','1','016f3b29107ed03252e477b08445cec4','35857757f553273b1056c1cabdace7fb','2017-10-05 11:35:13',54.90,16.01),('186a0845cd1b679057c8b0ea86931e2f','1','ea260f972b87224bc68cc0c605e915fc','966cb4760537b1404caedd472cc610a5','2018-07-25 17:31:17',692.00,13.00),('1a9b986cfe31ed59c5a6c00287db7e74','1','63de2743d05ab59d85caa49e49459ac1','cbd996ad3c1b7dc71fd0e5f5df9087e2','2017-08-10 16:23:39',19.90,11.85),('1c4f77cb28067456e266e0660dd20bdb','1','4fd6ec612afa6f67b850b157f00a3368','688756f717c462a206ad854c5027a64a','2018-07-05 10:30:18',26.90,18.29),('1e5b9ba96bdd2f8c7836f6fb5c7f9712','1','7825a7c4f9edb6b219eb94c29e86e7b9','de23c3b98a88888289c6f5cc1209054a','2017-12-22 14:11:27',169.90,16.95),('1f48aeb449520a42d9d2e7e1783e5a55','1','b56bf84f9384a87a3fbadd486d165615','8b321bb669392f5163d04c59e235e066','2018-03-15 12:30:35',13.65,7.39),('1fe7be398f57b2c40691376b673de1bb','1','869c418f22e0c0ed9207ff691f279e46','e63e8bfa530fb16910dd6956e592bb81','2017-10-26 11:07:13',32.90,17.63),('20109f40b1a81ce4ec90a30f1e625046','1','c4282015d858b8d5c14c1ec6b9000664','38102b031c2a15e54623d711bfc753d3','2017-07-27 09:35:18',39.00,44.20),('2011a926846f9fa10088488fca493f95','1','1317921ea764ef06f7aa21094c7e7759','7b07b3c7487f0ea825fc6df75abd658b','2017-11-30 15:52:31',19.90,16.79),('217fa08ee792f84b2a3b3a84ee263b0e','1','4a300735bc293723103db0d0c1bc1585','6f892e20a171e98efe17fdb971ff319b','2017-10-04 01:07:08',70.90,15.94);
/*!40000 ALTER TABLE `order_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_payments`
--

DROP TABLE IF EXISTS `order_payments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_payments` (
  `order_id` varchar(32) NOT NULL,
  `sequential` int(11) DEFAULT NULL,
  `method` varchar(30) DEFAULT NULL,
  `installments` int(11) DEFAULT NULL,
  `amount` decimal(8,2) DEFAULT NULL,
  KEY `order_payments_order_idx` (`order_id`),
  KEY `order_payments_method_idx` (`method`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_payments`
--

LOCK TABLES `order_payments` WRITE;
/*!40000 ALTER TABLE `order_payments` DISABLE KEYS */;
INSERT INTO `order_payments` VALUES ('1594012ccc1b0770373ce691d697e5ae',1,'credit_card',8,362.17),('1a9b986cfe31ed59c5a6c00287db7e74',1,'credit_card',1,31.75),('1314353395f7148ba64400eab34bb50c',1,'credit_card',10,156.73),('1422a4b4a2aa8b66fd49ff5557200b42',1,'credit_card',10,924.12),('1355bd6c7fa80ea43bdecab48ff8052c',1,'credit_card',1,85.15),('0e5f3862d95c44aabc9b2a0f703f9f75',2,'voucher',1,50.00),('0d2d1445891e7aba669fa2069d695601',1,'boleto',1,51.75),('12e6f847bdf86df79a51931bbd0798b9',1,'credit_card',1,73.16),('20109f40b1a81ce4ec90a30f1e625046',2,'voucher',1,78.01),('2011a926846f9fa10088488fca493f95',1,'credit_card',1,36.69),('131bcb82ecd96d251fa7eb8bb1f1baca',1,'credit_card',1,375.25),('05f3fc274d71ebbd552966d716c9039d',1,'credit_card',2,167.97),('02f2f6063eb01b5e6e29bda347635389',1,'boleto',1,56.39),('178358411049ad27f877b8964564528a',6,'voucher',1,12.00),('186a0845cd1b679057c8b0ea86931e2f',1,'credit_card',1,705.00),('217fa08ee792f84b2a3b3a84ee263b0e',1,'credit_card',2,86.84),('10f69c462a2c700cf1b141b9874af717',1,'credit_card',1,20.38),('084051935208c688c8cd5fae3d15810d',1,'voucher',1,62.68),('16ec5f5b0b5245798a6334e4c239d13a',1,'credit_card',6,123.52),('0f95a7e23723d5388a494f2d812a8332',1,'credit_card',1,35.13),('1f48aeb449520a42d9d2e7e1783e5a55',1,'credit_card',2,21.04),('0a0b0479c9e05814f250c7bea5b1a95c',1,'credit_card',3,274.54),('08915d73ff742886f0b4c9429af3c959',1,'boleto',1,98.16),('1e5b9ba96bdd2f8c7836f6fb5c7f9712',1,'credit_card',1,186.85),('14e29f83cf6fcd5fcca40380a248eea1',1,'credit_card',1,72.98),('1194afa31e54aeee123be8df42e3f573',1,'credit_card',4,94.55),('09f58c00f941827ab206de7796785e44',1,'boleto',1,17.62),('1c4f77cb28067456e266e0660dd20bdb',1,'credit_card',1,45.19),('011f9dff2545a2cf8ac1809faed3ec88',1,'credit_card',10,126.26),('0daf1f0e67b534ad873ba61c5d6ad7d3',1,'credit_card',5,153.20),('1fe7be398f57b2c40691376b673de1bb',1,'credit_card',5,50.53),('04a59fe46fc5aa341da5378c195ed53c',1,'credit_card',1,75.18),('0acfe927eda045512670dbbf763b330a',1,'credit_card',10,114.79),('053fdca16d1f69bd52f908f070a19049',1,'credit_card',3,69.71),('14de412ae0efc05d642771ab3bff5da6',1,'credit_card',1,173.90),('17afedb7e5fad837f54bacf5c66c99f8',1,'credit_card',6,70.91);
/*!40000 ALTER TABLE `order_payments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `id` varchar(32) NOT NULL,
  `customer_id` varchar(32) NOT NULL,
  `status` varchar(20) NOT NULL,
  `purchased_at` datetime DEFAULT NULL,
  `approved_at` datetime DEFAULT NULL,
  `pickedup_at` datetime DEFAULT NULL,
  `delivered_at` datetime DEFAULT NULL,
  `estimated_delivery` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_customer_idx` (`customer_id`),
  KEY `order_status_idx` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES ('011f9dff2545a2cf8ac1809faed3ec88','fd10113c54f4f4bfcaacbd1c2d188a77','delivered','2017-08-01 19:00:07','2017-08-01 19:10:21','2017-08-03 12:41:48','2017-08-09 21:04:42','2017-08-23 00:00:00'),('02f2f6063eb01b5e6e29bda347635389','92ac4160874c75b187c8222aec92e1a6','delivered','2018-03-19 19:36:41','2018-03-20 03:10:37','2018-03-20 19:12:46','2018-03-24 00:38:35','2018-03-29 00:00:00'),('04a59fe46fc5aa341da5378c195ed53c','2d8748fb35d51c2fd273ee67fff78b7a','delivered','2017-11-26 09:32:06','2017-11-28 09:35:11','2017-11-28 19:18:52','2017-12-11 19:03:18','2017-12-19 00:00:00'),('053fdca16d1f69bd52f908f070a19049','f90389504400442cf00ae1b04e8280a9','delivered','2017-10-07 13:59:13','2017-10-07 14:07:24','2017-10-09 20:43:04','2017-10-17 22:34:18','2017-11-03 00:00:00'),('05f3fc274d71ebbd552966d716c9039d','011671897080a38fa57f3c190a310d49','delivered','2018-05-11 18:36:13','2018-05-11 19:01:11','2018-05-15 13:58:00','2018-05-16 18:08:30','2018-05-22 00:00:00'),('084051935208c688c8cd5fae3d15810d','e7f806cb2cffddb308ed9bfd6bf5c5fd','delivered','2017-02-24 15:46:49','2017-02-24 16:02:42','2017-03-10 09:30:53','2017-03-28 16:52:33','2017-03-27 00:00:00'),('08915d73ff742886f0b4c9429af3c959','cd1e26832990fe789ebf64b0bd3de6c1','delivered','2018-04-22 21:23:13','2018-04-24 17:11:29','2018-04-26 07:49:00','2018-04-27 13:27:37','2018-05-11 00:00:00'),('09f58c00f941827ab206de7796785e44','7c94da97db6fe83e123f80baab533a97','delivered','2017-01-05 16:05:07','2017-01-07 03:35:34','2017-01-11 15:47:40','2017-01-16 15:43:31','2017-02-13 00:00:00'),('0a0b0479c9e05814f250c7bea5b1a95c','bcce684969990851024216985d1a9b5c','delivered','2018-06-14 21:10:57','2018-06-14 21:36:48','2018-06-15 12:43:00','2018-06-21 19:52:01','2018-07-16 00:00:00'),('0acfe927eda045512670dbbf763b330a','c56ac9e8772a402f6a167237b0c13f28','delivered','2018-04-21 15:10:55','2018-04-24 18:58:50','2018-04-23 20:34:32','2018-04-27 13:31:48','2018-05-17 00:00:00'),('0d2d1445891e7aba669fa2069d695601','739ee2d2d5960609ad780937fd9fa282','delivered','2017-10-09 19:54:18','2017-10-11 03:49:33','2017-10-11 18:28:36','2017-10-16 16:46:59','2017-10-26 00:00:00'),('0daf1f0e67b534ad873ba61c5d6ad7d3','1681471e9172d7e12ba08e9ac9e8628b','delivered','2017-09-05 10:54:17','2017-09-06 11:03:50','2017-09-09 13:23:11','2017-09-19 15:35:03','2017-09-25 00:00:00'),('0e5f3862d95c44aabc9b2a0f703f9f75','573e026da782d51277f320fc94a8d4e8','delivered','2018-02-11 23:30:57','2018-02-11 23:50:30','2018-02-14 22:42:23','2018-02-23 00:43:15','2018-03-09 00:00:00'),('0f95a7e23723d5388a494f2d812a8332','5f454675eda0812f4ca038d7a6796a38','delivered','2018-04-01 19:50:42','2018-04-01 20:08:34','2018-04-03 21:19:23','2018-04-12 00:24:45','2018-04-25 00:00:00'),('10f69c462a2c700cf1b141b9874af717','641d5bf17c23bb54add76ce8e47c1bd2','delivered','2018-07-18 16:32:10','2018-07-18 16:42:33','2018-07-20 15:06:00','2018-07-23 19:32:29','2018-07-31 00:00:00'),('1194afa31e54aeee123be8df42e3f573','b383a778278d061c8b275a611c26ca8a','delivered','2018-06-05 16:56:38','2018-06-05 17:15:38','2018-06-07 08:38:00','2018-06-13 18:48:54','2018-07-11 00:00:00'),('12e6f847bdf86df79a51931bbd0798b9','0ca135c2d4b81bb0234b5db12d9b7a8f','delivered','2017-07-16 21:37:40','2017-07-16 21:50:16','2017-07-17 16:37:26','2017-07-25 02:39:38','2017-08-10 00:00:00'),('1314353395f7148ba64400eab34bb50c','3a38cfdf41dbda6098abeacd30ec2d3e','delivered','2017-11-27 09:22:59','2017-11-27 09:33:29','2017-11-27 16:33:11','2017-12-15 21:29:37','2017-12-20 00:00:00'),('131bcb82ecd96d251fa7eb8bb1f1baca','24dd6be58b4717018fa8062703561b76','delivered','2017-09-14 10:29:33','2017-09-14 10:45:13','2017-09-18 23:54:41','2017-10-07 11:37:32','2017-10-02 00:00:00'),('1355bd6c7fa80ea43bdecab48ff8052c','934a9204aacc7279a9db0896b83eb42e','delivered','2017-11-27 01:29:11','2017-11-27 01:37:23','2017-11-27 18:40:07','2017-12-04 20:49:08','2017-12-22 00:00:00'),('1422a4b4a2aa8b66fd49ff5557200b42','4b24f5ef0c134fe4d56bb392b3397dcd','delivered','2018-05-28 14:18:26','2018-05-28 14:35:44','2018-05-29 12:04:00','2018-06-04 16:03:45','2018-06-29 00:00:00'),('14de412ae0efc05d642771ab3bff5da6','b5983676b44fc48efb58264d12ffce30','delivered','2018-04-23 14:23:32','2018-04-24 18:46:37','2018-04-24 19:36:43','2018-04-25 19:57:32','2018-05-02 00:00:00'),('14e29f83cf6fcd5fcca40380a248eea1','d1d326edcb5fc3e9aadd6e86b7a57910','delivered','2018-02-13 11:28:54','2018-02-13 12:30:27','2018-02-16 17:49:34','2018-02-18 12:45:08','2018-03-01 00:00:00'),('1594012ccc1b0770373ce691d697e5ae','e957d597f76d275f40a848397fb6c7c3','delivered','2018-04-23 19:05:57','2018-04-24 19:17:00','2018-04-24 12:48:39','2018-04-27 22:08:35','2018-05-08 00:00:00'),('16ec5f5b0b5245798a6334e4c239d13a','78463bfeb649747e91ee9a424d0edc3c','delivered','2018-01-22 10:46:14','2018-01-22 14:16:44','2018-01-24 22:38:45','2018-02-05 11:43:00','2018-02-15 00:00:00'),('178358411049ad27f877b8964564528a','71b3c28ced6fa14321a92998ecd717e2','delivered','2018-03-05 10:10:02','2018-03-05 11:00:25','2018-03-06 17:57:42','2018-03-14 21:16:53','2018-03-22 00:00:00'),('17afedb7e5fad837f54bacf5c66c99f8','ad4950aded55c2ea376be59506456d68','delivered','2017-10-01 11:21:12','2017-10-01 11:35:13','2017-10-03 20:53:07','2017-10-10 21:52:04','2017-10-24 00:00:00'),('186a0845cd1b679057c8b0ea86931e2f','a820146409b46b535505aff7d58f3ffc','delivered','2018-07-19 17:22:40','2018-07-19 17:35:08','2018-07-20 14:52:00','2018-07-23 23:22:25','2018-07-31 00:00:00'),('1a9b986cfe31ed59c5a6c00287db7e74','377e81546d7d92c97c710a231e747c45','delivered','2017-08-06 16:04:38','2017-08-06 16:23:39','2017-08-07 20:17:39','2017-08-15 17:14:54','2017-08-25 00:00:00'),('1c4f77cb28067456e266e0660dd20bdb','60840dcf2701955cedc2aff564f80c3e','delivered','2018-06-30 10:19:31','2018-06-30 10:30:18','2018-07-04 13:07:00','2018-07-10 16:31:49','2018-07-25 00:00:00'),('1e5b9ba96bdd2f8c7836f6fb5c7f9712','e0f38b37c6f511a9d6219102bb2e932d','delivered','2017-12-18 12:49:54','2017-12-18 14:11:27','2017-12-20 00:13:41','2017-12-26 19:54:07','2018-01-15 00:00:00'),('1f48aeb449520a42d9d2e7e1783e5a55','564f0d69cba2da3f21f9017402d0dc06','delivered','2018-03-10 12:23:32','2018-03-10 12:30:35','2018-03-12 21:21:50','2018-03-15 23:48:41','2018-03-22 00:00:00'),('1fe7be398f57b2c40691376b673de1bb','6f1b9db6eb561df558883985dbd941fc','delivered','2017-10-19 10:17:42','2017-10-20 11:07:13','2017-10-23 15:39:05','2017-11-01 17:12:21','2017-11-14 00:00:00'),('20109f40b1a81ce4ec90a30f1e625046','ce5e2dbebc82fdeb89bb35dfaddd56e2','delivered','2017-07-21 09:22:49','2017-07-21 09:35:18','2017-08-08 13:56:51','2017-08-14 14:55:07','2017-08-24 00:00:00'),('2011a926846f9fa10088488fca493f95','e3f8a41dc8def48711c086dad89ab9fd','delivered','2017-11-24 13:25:13','2017-11-24 15:52:31','2017-11-24 22:38:47','2017-12-05 14:03:45','2017-12-22 00:00:00'),('217fa08ee792f84b2a3b3a84ee263b0e','804751ae168c4c4d6d5178553e544300','delivered','2017-09-26 00:51:00','2017-09-26 01:07:08','2017-09-29 15:19:10','2017-10-28 15:35:59','2017-10-25 00:00:00');
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `id` varchar(32) NOT NULL,
  `category` varchar(100) NOT NULL,
  `name_len` int(11) DEFAULT '0',
  `desc_len` int(11) DEFAULT '0',
  `photos` int(11) DEFAULT NULL,
  `weight` int(11) DEFAULT NULL,
  `length` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `product_category_idx` (`category`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES ('016f3b29107ed03252e477b08445cec4','automotivo',56,871,2,3500,25,5,25),('097d5a4bed2be012b4e46e6f5ae0694b','beleza_saude',60,1162,1,833,16,12,18),('126abb319a746b23254ef06ddf10181d','relogios_presentes',58,335,5,450,16,2,11),('1317921ea764ef06f7aa21094c7e7759','esporte_lazer',26,438,2,200,16,6,20),('16ed6a6e3fce23b741650437fe58d65b','utilidades_domesticas',49,1226,1,750,35,15,35),('17a019676883dce326999c11a46a14f0','instrumentos_musicais',50,2143,4,150,16,4,14),('1f30aacc69904c0d0bf8d05cb2abef72','cama_mesa_banho',58,293,1,1850,40,10,30),('2a15783d3cb601f8d9a6a86806b037b3','bebes',47,1080,1,600,38,5,33),('3a44e1cd6610483f2ab5e0ca8c2a4b45','brinquedos',60,1051,4,700,35,40,25),('4a300735bc293723103db0d0c1bc1585','telefonia',42,661,4,250,16,4,20),('4fd6ec612afa6f67b850b157f00a3368','ferramentas_jardim',45,1095,9,598,38,30,38),('55ba658d5c2f0b1f118f98c36fe3fa04','brinquedos',63,2136,1,200,16,2,11),('5f2641478d7b5ffbfcf1f2b5cdff4709','utilidades_domesticas',60,1713,3,1950,102,8,11),('63de2743d05ab59d85caa49e49459ac1','beleza_saude',51,951,1,178,18,11,17),('65266b2da20d04dbe00c5c2d3bb7859e','papelaria',38,316,4,250,51,15,15),('6f33a4a09ae1180a0ee1ff4682b2d21f','brinquedos',49,426,7,700,44,12,22),('72d3bf1d3a790f8874096fcf860e3eff','brinquedos',57,341,2,583,20,21,20),('7825a7c4f9edb6b219eb94c29e86e7b9','papelaria',59,504,6,800,22,7,15),('869c418f22e0c0ed9207ff691f279e46','utilidades_domesticas',27,282,2,300,20,10,15),('89db899bdb35ae40d0901e38f21d2ede','automotivo',46,458,1,2200,16,2,11),('8bb27b1d96be90b36b8d0c7f30931d52','utilidades_domesticas',30,622,1,5950,33,26,33),('8d1224321dc883703df71dcb5be296b3','cama_mesa_banho',57,630,2,750,55,14,38),('94774f52c71a3e4ce084c21c873507cc','automotivo',50,296,2,100,16,10,15),('99a4788cb24856965c36a24e339b6058','cama_mesa_banho',54,245,1,1383,50,10,40),('9f7b48e2d4e4d7375f71a10791faa1b8','cama_mesa_banho',42,507,4,2050,50,10,40),('a62e25e09e05e6faf31d90c6ec1aa3d1','relogios_presentes',58,3006,2,1000,53,8,18),('b56bf84f9384a87a3fbadd486d165615','eletronicos',40,363,1,150,25,7,16),('b6c312bb8b2e0c3ab0a23d207802f82f','cama_mesa_banho',58,395,4,5967,48,37,53),('b8ca27a8b7658a95a8040f0a9489c952','cama_mesa_banho',59,577,2,10300,51,21,51),('c4282015d858b8d5c14c1ec6b9000664','moveis_decoracao',46,1195,3,850,40,36,28),('d02e5b014cc50a42a3ede0b3dc1fc63e','utilidades_domesticas',44,203,1,1100,19,22,19),('d103baa8d35783ff2772704ed17ddd45','fashion_bolsas_e_acessorios',53,553,5,250,16,2,20),('ea260f972b87224bc68cc0c605e915fc','relogios_presentes',44,170,2,343,19,22,20),('ec529a51d3a0832578dc8ca51574d2bd','moveis_decoracao',53,682,1,250,32,2,22),('ed2067a9c1f79553088a3c67b99a9f97','alimentos',52,919,1,750,16,12,15),('fbc1488c1a1e72ba175f53ab29a248e8','perfumaria',27,311,1,560,19,13,17);
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sellers`
--

DROP TABLE IF EXISTS `sellers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sellers` (
  `id` varchar(32) NOT NULL,
  `zipcode` varchar(10) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  `state` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sellers_city_idx` (`city`),
  KEY `sellers_state_idx` (`state`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sellers`
--

LOCK TABLES `sellers` WRITE;
/*!40000 ALTER TABLE `sellers` DISABLE KEYS */;
INSERT INTO `sellers` VALUES ('259f7b5e6e482c230e5bfaa670b6bb8f','08550','poa','SP'),('2a1348e9addc1af5aaa619b1a3679d6b','30494','belo horizonte','MG'),('3785b653b1b82de85ab47dd139938091','85640','ampere','PR'),('38102b031c2a15e54623d711bfc753d3','62170','mucambo','CE'),('6560211a19b47992c3666cc44a7e94c0','05849','sao paulo','SP'),('723cd880edaacdb998898b67c8f9da30','14940','ibitinga','SP'),('7b07b3c7487f0ea825fc6df75abd658b','02016','sao paulo','SP'),('8b321bb669392f5163d04c59e235e066','01212','sao paulo','SP'),('966cb4760537b1404caedd472cc610a5','09920','diadema','SP'),('9f505651f4a6abe901a56cdc21508025','04102','sao paulo','SP'),('b33aaadd1a8891d2dfef0c4c3bdf0371','13481','limeira','SP'),('bd697db56ff8e7c9cd1abeb7f72029b2','03533','sao paulo','SP'),('cbd996ad3c1b7dc71fd0e5f5df9087e2','15081','sao jose do rio preto','SP'),('e5a38146df062edaf55c38afa99e42dc','01233','sao paulo','SP'),('e63e8bfa530fb16910dd6956e592bb81','07160','guarulhos','SP'),('ef0ace09169ac090589d85746e3e036f','24451','sao goncalo','RJ');
/*!40000 ALTER TABLE `sellers` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-07-27 20:55:10