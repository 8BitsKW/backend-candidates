<?php
use DI\Container;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Slim\Factory\AppFactory;


require './vendor/autoload.php';

$container = new Container;
$container->set('db', function(){
    $pdo = new PDO("mysql:host=db;dbname=bits_store", "bits_store_user", "bits_store_pwd");
    return $pdo;
});

$container->set('guzzle', function(){
    return new GuzzleHttp\Client([
        'base_uri' => 'https://swapi.dev/api/'
    ]);
});

$app = AppFactory::createFromContainer($container);
$app->addErrorMiddleware(true, false, false);


$app->get('/', function (Request $request, Response $response, array $args) {
    $db      = $this->get('db');
    $count = $db->query('SELECT count(*) c from orders')->fetch(PDO::FETCH_OBJ);
    
    if (trim($count->c) != 36){
        throw new \Exception("Failed to connect to DB Server and fetch the right orders");
    }

    $swapi         = $this->get('guzzle')->get('people/');
    if ($swapi->getStatusCode() != 200){
        throw new \Exception("Failed to get api from guzzle");
    }
    
    
    $html = file_get_contents("public/readme.html");
    $response->getBody()->write($html);
    return $response;
});



$app->run(); 