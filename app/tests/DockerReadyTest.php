<?php

use PHPUnit\Framework\TestCase;

class DockerReadyTest extends TestCase
{

    private $http;

    public function setUp() : void {
        $this->http = new GuzzleHttp\Client(['base_uri' => 'http://localhost:80']);
    }


    public function testGet()
    {
        $response = $this->http->request('GET', '/');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function tearDown() : void {
        $this->http = null;
    }
}