#/bin/bash

if [ -d "vendor" ]; then rm -Rf vendor; fi
composer install 2> /dev/null

php-fpm -D 2> /dev/null
echo "APP is now Ready to use"
nginx -g 'daemon off;'
